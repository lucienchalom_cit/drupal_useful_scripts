# Drupal Useful scripts
We have a collection of useful scripts to help you create a local environment and a Drush fresh installation in no time. Enjoy!

## Drupal Local Environment Setup

This script will prepare your local environment (Linux Ubuntu or Mac OS) getting you ready to develop in Drupal Projects. It will install all you need following best practices established by Drupal Competence Office (DCO) at CI&T.

### What will be installed

This script will install:

* **Docker** (https://www.docker.com/)
* **Lando** (https://lando.dev/)
* **VSCode** (https://code.visualstudio.com/)
* Extensions for VSCode:
    Docker, Composer, Apache Conf Snippets, Drupal 8 Twig Snippets, Drupal 8 Snippets, Drupal 8 JavaScript Snippets, File Utils, PHP Debug, PHP Intelephense, phpcs, PHP DocBlocker
* **Composer** (https://getcomposer.org/)
* **PHP7.4** (https://www.php.net/) or **PHP8.0** (Just for Linux)
* **PHP-cli** (http://www.php-cli.com/)
* **PHPCS** (https://github.com/squizlabs/PHP_CodeSniffer)
* **PHPCBF** (https://github.com/squizlabs/PHP_CodeSniffer/wiki/Fixing-Errors-Automatically#using-the-php-code-beautifier-and-fixer)


## Drupal Site Installation and Configuration

This script will prepare your recently setup local environment (Linux Ubuntu or Mac OS) downloading fresh Drupal install and intalling it with minimum interaction from you.

With this script you will be able to have a clean installation choosing between Umami Demo and Standard installation.

# Step-by-Step

### Installation

Download this script to your local host and change its permissions via terminal using the following command:

```bash
sudo chmod u+x drupal.sh
sudo chmod u+x drupal-drush-install.sh
```

### Usage
In order to run the script you need to run the following code

```bash
sh drupal.sh; sh drupal-drush-install.sh
```
It will then eventually ask for your password and a few inputs from you, so you allow the installation process to go on. This is interactive and you are a crucial part of it, so don't get distracted :)

### Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### License
[GPLv3](https://choosealicense.com/licenses/gpl-3.0/)

### Acknowledgments

This project had the indispensable help of:

* Gabriel Abdalla (abdalla@ciandt.com)
* Paulo Henrique Cota Starling (paulocs@ciandt.com)
* Marcus Vinicius Alves da Silva Souza (marcuss@ciandt.com)

Honorable mentions to suggestions made by the entire DCO team.